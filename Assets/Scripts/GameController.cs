using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.UI;

using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public Text enemysText;
    public Text lifesText;
    public Text timeText;
    

    private int _enemys = 5;
    private int _lifes = 3;
    private float _time = 45;
    
    private void Start()
    {
        
        enemysText.text = "Enemigos: " + _enemys;
        lifesText.text = "Vidas: " + _lifes;
        
    }

    void Update()
    {
        if (timeText!=null)
        {
            _time -= Time.deltaTime;
            timeText.text = "Tiempo: " + _time;
        }
        if (_time <= 0)
        {
            SceneManager.LoadScene("Nivel2");
        }
    }

    public int GetScore()
    {
        return _enemys;
    }

    public void DestroyEnemy()
    {
        _enemys -= 1;
        enemysText.text = "Enemigos: " + _enemys;
    }

    public void LoseLife()
    {
        _lifes -= 1;
       lifesText.text = "Vidas: " + _lifes;
    }

    public int GetLifes()
    {
        return _lifes;
    }
    public int GetEnemys()
    {
        return _enemys;
    }

    
}
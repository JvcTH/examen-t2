using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;
public class RobotController : MonoBehaviour
{
    private Rigidbody2D rb;
    private Animator animator;
    private SpriteRenderer sr;
    public GameObject rightBullet;
    public GameObject leftBullet;

    public GameObject rightBulletCharge;
    public GameObject leftBulletCharge;
    
    private GameController _game;
    
    public float charge = 0;
    public float timeDeath = 0;
    public bool muerto = false;
    public int nivel = 1;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        sr = GetComponent<SpriteRenderer>();
        
        _game = FindObjectOfType<GameController>();
    }

    // Update is called once per frame
    void Update()
    {
        
        if (muerto)
        {
            animator.SetInteger("Estado",6);
            timeDeath += Time.deltaTime;
            if (timeDeath >= 1 )
            {
                if (nivel == 1)
                {
                    SceneManager.LoadScene("SampleScene");
                }else{
                    SceneManager.LoadScene("Nivel2");
                }
            }
        }else
        {
            animator.SetInteger("Estado", 0);
            rb.velocity = new Vector2(0, rb.velocity.y); 

            if (Input.GetKey(KeyCode.RightArrow))
            {
                rb.velocity = new Vector2(6, rb.velocity.y); 
                sr.flipX = false;
                animator.SetInteger("Estado",1); 
            }
            if (Input.GetKey(KeyCode.LeftArrow))
            {
                rb.velocity = new Vector2(-6, rb.velocity.y); 
                sr.flipX = true; 
                animator.SetInteger("Estado",1); 
            }
            if (Input.GetKeyUp(KeyCode.Space))
            {
                rb.AddForce(Vector2.up * 40, ForceMode2D.Impulse);
                
                animator.SetInteger("Estado",2); 
            }   
            
            if (Input.GetKey(KeyCode.Z))
            {
                animator.SetInteger("Estado",3); 
            } 
            if (Input.GetKey(KeyCode.X)){
                charge += Time.deltaTime;
                
                if (sr.color == Color.green)
                {
                    sr.color= Color.white;
                }
                sr.color= Color.green;
            }   
            if (Input.GetKeyUp(KeyCode.X))
            {
                if (sr.color == Color.green)
                {
                    sr.color= Color.white;
                }
                if (rb.velocity.x == 0)
                {
                    animator.SetInteger("Estado",4); 
                }else
                {
                    animator.SetInteger("Estado",5); 
                }
                if (charge >= 2)
                {
                    var bullet = sr.flipX ? leftBulletCharge : rightBulletCharge;
                    var position = new Vector2(transform.position.x, transform.position.y);
                    var rotation = rightBulletCharge.transform.rotation;
                    Instantiate(bullet, position, rotation);
                }else
                {
                    var bullet = sr.flipX ? leftBullet : rightBullet;
                    var position = new Vector2(transform.position.x, transform.position.y);
                    var rotation = rightBullet.transform.rotation;
                    Instantiate(bullet, position, rotation);
                }
                charge = 0;
                
                
            }
        }
        
        
    }
   
    private void OnCollisionEnter2D(Collision2D collision)
    {
        
        if (collision.gameObject.CompareTag("enemy"))
        {
            
            _game.LoseLife();
            if (_game.GetLifes()<=0)
            {
                muerto=true;
            }
            
        }
        
    }
    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.CompareTag("key"))
        {
            
            if (_game.GetEnemys() == 0)
            {
                if (nivel == 1)
                {
                    SceneManager.LoadScene("Nivel2");
                }else{
                    SceneManager.LoadScene("Final");
                }
               
            }
            
        }
    }

}